clear;clc;
%V0=miu0*Kr*Ui
%Ui=9.49
V0 = 0.5*0.2*21.04;
A = 8;
ku = 0.62;
I21=8;
U21 = 24;
L = 0.64;
C = 8.14e-4;
Rs = U21/I21;
kr=0.23;
%H=tf([1],[L*C L/Rs 1]);
Hf = tf([1/(2*A)*sqrt(2)*ku*U21], [L*C L/Rs 1]);
bode(Hf)

% step(feedback(Hf,kr));
% %%
% %P
% 
 phi=-180+50+15;
% 
 w_p=200;
 k_p=1/db2mag(-31);
% 
 Hc_p=k_p;
% 
 Hdes_p=series(Hf,Hc_p);
 figure,
 bode(Hdes_p);

% % % % %%
% % % % %PI
% % % % 
  phi_pi=-180+15+65;
% %  
  w_pi=94.9;
% % % 
  k_pi=1/db2mag(7.26);
% % % 
 Ti=4/w_pi;
% % % 
  Hc_pi=tf([k_pi*Ti k_pi],[Ti 0]);
% % % 
 Hdes=series(Hf,Hc_pi);
%   figure,
%   bode(Hdes);
% % % % % 
% % % % % %%
% % % % % %PD
  beta=0.1;
 phi_d=-180-atand((1-beta)/(2*sqrt(beta)))+65;
% % % 
 w=1.94e3;
% % % 
  Td=1/(w*sqrt(beta));
% % % 
 k=sqrt(beta)*db2mag(-69);
% % % 
  Hc=tf(k*[Td 1],[Td*beta 1]);
% % % 
 Hdes=series(Hf,Hc);
% %  figure,
% bode(Hdes);