public class DeluxeBurger extends BasicBurger
{
    public DeluxeBurger(float price, int meat) {
        super(7, meat, 5);
    }

    @Override
    public double totul(boolean if_bacon, boolean if_sauce, boolean if_pickles, boolean if_cheese, int maxim, int i) {
        return super.totul(if_bacon, if_sauce, if_pickles, if_cheese, maxim, i);
    }
}
