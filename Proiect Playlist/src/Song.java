public class Song
{
    private String title;// privat nu ma lasa sa accesez din alte clase
    private double duration;

    public Song(String song, double duration) {// constructor pentru a putyea accesa din alte clase titlul si durata melodiei dar sa fie in continuare private
        this.title = song;  //cu this accesez campurile clasei
        // in constructor pun ca parametri ce vreau sa accesz oricum daca sunt private datele
        this.duration = duration;
    }

    @Override
    public String toString() {
        return this.title+";"+this.duration;

    }//pentru a afisa mai usor obiectul // metoda predefinita care ma ajuta sa fac asta,

    public String getTitle() {
        return title;
    }// geterele sunt metode speciale care se folosesc pentru a putea returna campurile porivate dintr o clasa

    public double getDuration() {
        return duration;
    }
}
