import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Scanner;

/*
     Create a program that implements a playlist for songs
     Create a song class that has the title and song duration for a song
     The program will have an album class that has a list of songs
     The album will be stored in array list
     Songs from different albums can be added to the playlist and will appear in the list in the order they were added
     Once the songs have been added to the playlist create a menu of options to:
     -quit, Skip forward to the next song ,skip backward to the previous song,replay the current song
     list the songs in the playlist
     A song must exist in an album before it can be added to the playlist(so you can only play songs that you own)
     Hint : To replay a song ,consider what happened when we went back and forth from a city before we started tracking the direction we were going
     As an optional extra,provide an option to remove the current song from the playlist
     Hint: listiterator.remove()
 */
public class main
{ private static ArrayList<Album> albums=new ArrayList<Album>();
    public static void main(String[] args)
    {

        Album album=new Album("Multiply","Ed Sheeran");
        album.addSong("One",4.6);
        album.addSong("I'm a mess",4.1);
        album.addSong("Sing",3.6);
        album.addSong("Don't",3.5);
        album.addSong("Nina",2.9);
        album.addSong("Photograph",3.7);
        album.addSong("Bloodstream",3.9);
        album.addSong("Tenerife Sea",5.0);
        album.addSong("Runaway",4.9);
        albums.add(album);

        album =new Album("Semiternal","Bring me the horizon");
        album.addSong("Can you feel my heart?",3.33);
        album.addSong("The house of wolves",3.33);
        album.addSong(" Empire",3.31);
        album.addSong("Sleepwalking",3.50);
        album.addSong("Shadow Moses",3.40);
        album.addSong("Seen it all before",3.44);
        album.addSong("Antivist",3.00);
        albums.add(album);
        LinkedList<Song>playlist=new LinkedList<Song>();
        albums.get(0).addToPlaylist("Photograph",playlist);
        albums.get(1).addToPlaylist("Antivist",playlist);
        albums.get(0).addToPlaylist("Runaway",playlist);
        albums.get(0).addToPlaylist("Bloodstream",playlist);
        albums.get(1).addToPlaylist("Can you feel my heart?",playlist);
        albums.get(1).addToPlaylist("Sleepwalking",playlist);
        albums.get(1).addToPlaylist(5,playlist);
        play(playlist);




    }
    private static  void play(LinkedList<Song>playlist)
    {
        Scanner scanner=new Scanner(System.in);
        boolean quit=false;
        boolean forward=true;
        ListIterator<Song> listIterator=playlist.listIterator();
        if(playlist.size()==0)
        {
            System.out.println("No songs in playlist");
            return;
        }
        else {
            System.out.println("Now playing "+listIterator.next().toString());
            printMenu();
        }
        while(!quit)
        {
            int action=scanner.nextInt();
            scanner.nextLine();
            switch (action) {
                case 0:
                    System.out.println("Playlist Complete");
                    quit =true;
                    break;
                case 1:
                    if (!forward)
                    { if (listIterator.hasNext()) {
                            listIterator.next();
                        }
                    forward = true;
            }
                    if(listIterator.hasNext())
                    {
                        System.out.println("Now playing "+listIterator.next().toString());
                    }
                    else {
                        System.out.println("We have reached the end of the playlist");
                        forward=false;
                    }
                    break;
                case 2:
                    if(forward)
                    {
                        if(listIterator.hasPrevious())
                        {
                            listIterator.previous();
                        }
                        forward=false;
                    }
                    if(listIterator.hasPrevious())
                    {
                        System.out.println("Now playing "+listIterator.previous().toString());
                    }
                    else{
                        System.out.println("You are and the start of the playlist");
                        forward=true;
                    }
                    break;
                case 3:
                    if (forward)
                    {if(listIterator.hasPrevious()) {
                        System.out.println("Replaying " + listIterator.previous().toString());
                        forward=false;
                    }
                    else { System.out.println("We are at the start of the playlist");}}
                    else
                {
                    if(listIterator.hasNext())
                    {
                        System.out.println("Replaying "+listIterator.next().toString());
                        forward=true;
                    }
                    else System.out.println("we have reached the end of the list");
                }
                    break;
                case 4:
                    printList(playlist);
                    break;
                case 5:
                    printMenu();
                    break;
                case 6:
                    if(playlist.size()>0)
                        listIterator.remove();
                    if(listIterator.hasNext())
                        System.out.println("Now playing "+listIterator.next());
                    else if(listIterator.hasPrevious())
                        System.out.println("Now playing "+listIterator.previous());
                    break;
            }
        }
    }
        public static void printList(LinkedList<Song>playlist)
        {
            ListIterator<Song> iterator=playlist.listIterator();
            while(iterator.hasNext())
            {
                System.out.println("Now listening "+iterator.next().toString());
            }
        }
        public static void printMenu()
        {
            System.out.println("Available options: ");
            System.out.println("0 -Exit the playlist ");
            System.out.println("1 -Play the next song ");
            System.out.println("2 -Play the previous song ");
            System.out.println("3 -Replay the actual song ");
            System.out.println("4 -Show playlist ");
            System.out.println("5 -Show the menu ");
            System.out.println("6 -Delete current song from the playlist ");
        }

}
