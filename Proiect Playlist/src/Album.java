import java.util.ArrayList;
import java.util.LinkedList;

public class Album
{   private String name;
private String artist;
    private ArrayList<Song>albumSongs;//declarare lista java  intre triunghiurile ala este tipul listei, la noi de tip Song ca am clasa predefinita song care presupune o melodie cu titlu si durata si dupa triunghiurile alea pun titlul liste ce vreau eu

    public Album(String name,String artist) {
        this.name = name;
        this.artist=artist;
        this.albumSongs = new ArrayList<Song>() ;// aloc memorie pt lista mea
    }

    public String getName() {
        return name;
    }

    public ArrayList<Song> getAlbumSongs() {
        return albumSongs;
    }

    public boolean contains(Song melodie)
    {
        for(int i=0;i<albumSongs.size();i++)
            if(albumSongs.get(i).equals(melodie))
                return true;
            return false;
    }
    public boolean addSong(String title,Double duration)
      {
         if(findSong(title)==null)
         {
             this.albumSongs.add(new Song(title,duration));
             return true;
         }
           return false;
       }
       private Song findSong(String title)
       {
           for(Song checkedSong:this.albumSongs)//un for each,  nu mai e necesar sa punem indice si marime campului,merge si face o variabila numita checked song pt fiecare intrare
           {
               if(checkedSong.getTitle().equals(title))
               {
                   return checkedSong;
               }
           }
           return null;
       }
       public boolean addToPlaylist(int trackNumber, LinkedList<Song>playlist)
       {
           int index=trackNumber-1;
           if((index >0) && (index<=this.albumSongs.size()))
           {
               playlist.add(this.albumSongs.get(index));
               return true;
           }
           System.out.println("This album does not have a track "+trackNumber);
           return false;
       }
       public boolean addToPlaylist(String title,LinkedList<Song>Playlist)
       {
           Song checkedSong=findSong(title);
           if(checkedSong!=null)
           {
               Playlist.add(checkedSong);
               return true;
           }
           System.out.println("The song "+title+" is not in this album");
           return false;

       }
}
