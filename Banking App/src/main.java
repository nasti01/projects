/*
    -Create a simple banking application
    -there should be a bank class
    -it should have an array list of branches
    -each branch should have an array list of Customers
    -the customers class should have an array list of doubles(transactions)
    -Customer : Name, and array list of doubles
    -branch: need to be able to add a new customer and initial transaction amount
    -also needs to add additional transactions for that customer/branch
    -bank: add a new branch
    -add a customer to that branch with initial transaction
    -add a transaction for an existing customer for that branch
    -show a list of customers for a particular branch and optionally a  list of their transactions
    -Demonstrate autoboxing and unboxing in your code
    -add data validation
    -eg check if it exists or it doesn't

 */
public class main {
    public static void main(String[] args)
    {
      Bank bank=new Bank("Tiriak Bank");
      bank.addBranch("Piatra-Neamt");
      bank.addCustomer("Piatra-Neamt","Anastasia",10000.40);
      bank.addCustomer("Piatra-Neamt","Alex",20000);
      bank.addCustomerTransaction("Piatra-Neamt","Anastasia",900);
      bank.addCustomerTransaction("Piatra-Neamt","Alex",678);
      bank.addBranch("Cluj");
      bank.addCustomer("Cluj","Andreea",40000);
      bank.addCustomer("Cluj","Rares",456);
      bank.ListCustomers("Piatra-Neamt",true);
      bank.ListCustomers("Cluj",true);

    }
}
